import { Routes } from '@angular/router';

// Services
import { AuthGuard } from '../services';
import { PageComponent } from '@playground/ui-basic';
import { PageResolverService } from '@playground/ui-basic';

import { LoginService } from '../services';
import { CreateFormService } from '../services';

import * as login from '../../../configs/login.json';
import * as random from '../../../configs/random.json';
import * as navbar from '../../../configs/navbar.json';
import * as groomedForm from '../../../configs/groomed.json';
import * as builder from '../../../configs/builder.json';

const array = [random, login];

export const APP_ROUTES: Routes = [
  { path: 'builder', component: PageComponent,
    resolve: {
      data: PageResolverService
    }
  },
  {
    path: 'build', loadChildren: '@playground/page-builder/src/page-builder.module#PageBuilderModule',
    data: {
      config: builder
    },
    resolve: {
      data: PageResolverService
    }
  },
  { path: 'login', component: PageComponent,
    data: {
      config: login
    },
    resolve: {
      data: LoginService
    }
  },
  { path: 'forms', component: PageComponent,
    data: {
      config: navbar
    },
    canActivate: [AuthGuard],
    resolve: {
      data: LoginService
    },
    children: [
      { path: 'builder', component: PageComponent,
        data: {
          config: builder
        },
        resolve: {
          data: CreateFormService
        }
      },
      { path: 'groomed', component: PageComponent,
        data: {
          config: groomedForm
        },
        resolve: {
          data: LoginService
        }
      },
      { path: ':id', component: PageComponent,
        data: {
          key: null,
          config: {}
        },
        resolve: {
          data: LoginService
        }
      }
    ]
  },
  { path: 'forms/:id', component: PageComponent,
    data: {
      key: null,
      config: {}
    },
    resolve: {
      data: LoginService
    }
  },
  // { path: '**', redirectTo: '/builder', pathMatch: 'full' }
];
