import { Injectable, ComponentFactoryResolver } from '@angular/core';
import { RouterStateSnapshot, ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { ComponentBuilderService } from '@playground/ui-basic';
import { Observable }             from 'rxjs';

import { Router }                 from '@angular/router';



@Injectable()
export class CreateFormService implements Resolve<any> {

  page;
  
  constructor(
    private builder: ComponentBuilderService,
    private router: Router
  ){}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any>|Promise<any>|any {
    this.page = this.builder.build(route.data.config, this);
    console.log('this.page: ', this.page)
    return this.page;
  }

  createInput(e, ){
    console.log("new input: ", this.page);
    this.page.controls.push(this.builder.build({component: 'form', class: {shell: "w-25 bg-info"}, title: {name: 'hello', label: {name: "wtf we made it"}}}));
  }
}
