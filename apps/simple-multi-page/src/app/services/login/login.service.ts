import { Injectable, ComponentFactoryResolver } from '@angular/core';
import { RouterStateSnapshot, ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { ComponentBuilderService } from '@playground/ui-basic';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable }             from 'rxjs';
import { HttpClient } from '@angular/common/http';

import { Router }                 from '@angular/router';
import { AuthService } from '@playground/auth';

// We need to pass in app specific services here...
import * as config from '../../../../configs/login.json';
import * as random from '../../../../configs/random.json';


@Injectable()
export class LoginService implements Resolve<any> {

  constructor(
    private afAuth: AngularFireAuth,
    private db: AngularFirestore,
    private builder: ComponentBuilderService,
    private http: HttpClient,
    private router: Router,
    private authService: AuthService
  ){}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any>|Promise<any>|any {
    // console.log('************ - ', route, state);
    // const hey = this.http.get('configs/login.json').subscribe(response => {
    //   console.log('in here ', response);
    // });
    // console.log('hey!! ', data);
    // return this.db.doc('configs' + state.url)
    // .ref.get().then(doc => {
    //   console.log('doc ', doc.data())
    //   return this.builder.build(doc.data(),
    //   {
    //     loginForm: this
    //   });
    //   // We can use a pageService, to build the page
    //   // we send to page component.
    //   // return new PageService(config, events);
    //   // return {config: doc.data(), actions: this};
    // });
    if(route.data.key === null){
      const temp = this.builder.generate({}, this);
      // console.log('push to page now: ', temp);
      return temp;
    }else{
      console.log('building config:  ', route.data.config)
      return this.builder.build(route.data.config, this);
    }
  }

  greet(e, button, group, form){
    // console.log('WTF it\'s HELLO from Greet!!!', form);
    // const test = form.controls[0].controls[0]
    // console.log('test: ', test);
    // form.controls[0].controls.push(this.builder.testBuild(test));

    // const test = this.builder.testBuild({});
    // console.log('test is ', test);
  }

  login(e, button, group, form) {
    // console.log('form is valid? ', form.valid());
    const payload = form.payload();
    // console.log('payload: ', payload);
    // console.log('fjsljasjgkljsgjklsjglj ', this)
    this.afAuth.auth.signInWithEmailAndPassword(payload.email, payload.password).then(data => {
      this.authService.login(data.user);
      this.router.navigate(['/forms']);
      console.log('logged in return data: ', data);
    }).catch(function(error) {
      // Handle Errors here.
      // console.log("error logging in ", error);
      var errorCode = error.code;
      var errorMessage = error.message;
      // ...
    });

    // this.afAuth.auth.createUserWithEmailAndPassword(email, password).catch(function(error) {
    //   // Handle Errors here.
    //   console.log('error in login: ', error)
    //   // var errorCode = error.code;
    //   // var errorMessage = error.message;
    //   // ...
    //   }).then(data => {
    //   console.log('help!!! ', data)
    // })
  }
  logout(e, element, group, form) {
    console.log("logging out: ", form);
    this.afAuth.auth.signOut();
    this.authService.logout();
    this.router.navigate(['login']);
  }
}
