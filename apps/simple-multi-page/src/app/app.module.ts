// Core
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserModule } from '@angular/platform-browser';
import { NxModule } from '@nrwl/nx';
import { ReactiveFormsModule } from '@angular/forms';

// This App Modules
import { AppRoutingModule } from './app-routing/app-routing.module';

// Local libs
import { RandomTextModule } from '@playground/random-text';
import { ValidatorsModule } from '@playground/validators';
import { UiBasicModule } from '@playground/ui-basic';
import { AuthModule, AuthService } from '@playground/auth';

// Third party libs
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { environment } from '../environments/environment';
import { CreateFormService } from './services/create-form/create-form.service';
import { PageBuilderModule } from '@playground/page-builder';

@NgModule({
  imports: [
    BrowserModule,
    NxModule.forRoot(), // Nrwl.io
    NgbModule.forRoot(), // NG-Bootstrap
    RandomTextModule,
    ReactiveFormsModule,
    ValidatorsModule,
    UiBasicModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase, 'simple-multi-page'),
    AngularFirestoreModule,
    AngularFireAuthModule,
    AuthModule
  ],
  providers: [
    AuthService,
    CreateFormService
  ],
  declarations: [
    AppComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
