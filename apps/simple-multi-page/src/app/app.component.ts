import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  styleUrls: ['./app.component.css'],
  template: `
    <router-outlet></router-outlet>
  `
})

export class AppComponent implements OnInit {

  constructor() {
  }
  ngOnInit() {}
}
