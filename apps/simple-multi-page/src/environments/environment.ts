// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDqoyiHg8CEkMQ-iTX7LPQ-fTi0kJOpqPg",
    authDomain: "simple-multi-page.firebaseapp.com",
    databaseURL: "https://simple-multi-page.firebaseio.com",
    projectId: "simple-multi-page",
    storageBucket: "",
    messagingSenderId: "431136719957"
  }
};
