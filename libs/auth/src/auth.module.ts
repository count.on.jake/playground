import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthService } from './services/auth-service/auth-service';

@NgModule({
  imports: [
    CommonModule
  ]
})
export class AuthModule {
  static forRoot(authConfig): ModuleWithProviders {
    return {
      ngModule: AuthModule,
      providers: [
        AuthService
      ]
    };
  }
}
