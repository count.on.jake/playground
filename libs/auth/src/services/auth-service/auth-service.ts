import { Injectable } from '@angular/core';

@Injectable()
export class AuthService {

  user;
  // isLoggedIn = false;
  isLoggedIn = false;

  constructor() {}

  login(user){
    this.user = user;
    this.isLoggedIn = !!user;
  }

  logout(){
    this.user = null;
    this.isLoggedIn = false;
  }
}
