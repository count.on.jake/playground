import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { PageBuilderComponent } from './lib/page-builder/page-builder.component';
import { UiBasicModule, PageComponent } from '@playground/ui-basic';
import { PageBuilderResolverService } from './lib/page-builder-resolver/page-builder-resolver';

console.log('*************')
import * as builder from './configs/builder.json';

export const APP_ROUTES: Routes = [
  {path: '', component: PageComponent,
   children: [
     {path: 'page/:name', component: PageComponent,
       resolve: {
         data: PageBuilderResolverService
       }
     }
   ]
 },
 { path: '**', redirectTo: '', pathMatch: 'full' }
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(APP_ROUTES),
    UiBasicModule
  ],
  providers: [
    PageBuilderResolverService
  ],
  declarations: [PageBuilderComponent]
})
export class PageBuilderModule {}
