import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ComponentBuilderService } from '@playground/ui-basic';

@Component({
  selector: 'ui-page-builder',
  templateUrl: './page-builder.component.html',
  styleUrls: ['./page-builder.component.css']
})
export class PageBuilderComponent implements OnInit {

  config;
  constructor(private builder: ComponentBuilderService,
  private router: Router,
  private route: ActivatedRoute){}

  ngOnInit() {
  }

  newPage(){
    console.log('clicked new page', this.route);
    this.config = this.builder.build({component: 'page', name: 'newPageThing'});
    console.log('this router: ', this.router, this.config);
    this.router.navigate(['build/page', this.config.name]);
    this.route.snapshot.data = this.config;

  }
}
