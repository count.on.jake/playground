import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class TextService {

  textObject;
  URL = 'https://www.randomtext.me/api/lorem/';
  GIB = 'https://www.randomtext.me/api/gibberish/'
  constructor(private http: HttpClient){}

  get(requestString: string){
    return this.http.get(`${this.GIB}${requestString}`);
  }
}
