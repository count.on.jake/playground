import { TestBed, inject } from '@angular/core/testing';

import { ComponentBuilderService } from './component-builder.service';

describe('ComponentBuilderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ComponentBuilderService]
    });
  });

  it(
    'should be created',
    inject([ComponentBuilderService], (service: ComponentBuilderService) => {
      expect(service).toBeTruthy();
    })
  );
});
