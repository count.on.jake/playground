import { Page } from '../../models/page/page.model';
import { Form } from '../../models/form/form.model';
import { GroupBuilderService } from '../group-builder/group-builder.service';

// test idea get rid of then...
import { Input } from '../../models/input/input.model';
// import { Group } from '../../models/group/group.model';
import { Base } from '../../models/base/base.model';
import { SeedService } from '../seed/seed.service';

const compFactory = {
  page: Page,
  form: Form,
  input: Input
};

import { Injectable, ComponentFactoryResolver } from '@angular/core';

@Injectable()
export class ComponentBuilderService {
  constructor(
    private resolver: ComponentFactoryResolver,
    private GB: GroupBuilderService,
    private seed: SeedService
  ){}

  build(config, actions = {}) {
    // console.log('set config: ', config, actions);
    // console.log('actions says: ', actions.login.greet());
    // console.log('Its happening!!!!! ', this.registry);
    // console.log('build: ', actions);
    let temp = new compFactory[config.component](config, actions, this.GB);
    // console.log('this is temp!! ', temp);
    // WE need to make a page object.
    // Waht would a page object do?
    // How do we pass actions on?

    // const yellow = new actions();
    // console.log("hello yellow: ", yellow.greet)
    // We need to couple the actions, and iterate over
    // the controls of each nested group.
    // configs.component = this.resolver.resolveComponentFactory<any>(
    //   ButtonComponent
    // );
    // return new this.builder[config.component](config);
    // return new this.models[config.component.name](config);
    // config.component.definition = this.registry[config.component.name];
    // return config;
    // return {config: temp, actions: actions};
    return temp;
    // configs.component.definition
    // WE should create a new object that we can make
    // copies of. How would we save it?
    // new Page(config);
  }

  generate(object, actions){
    console.log("generate: ", actions);
    const seed = this.seed.build();
    // console.log('seed is here:::>> ', seed);
    let temp = new compFactory['page'](seed, actions, this.GB);
    return {config: temp, actions: actions};
  }
}

// 1. we unpack the page component.
// 2. unpack the page controls, which are form, table, etc...
// 3. unpack the controls of each form or table controls.

// A form will have one or more groups.
// A group will have one or more elements
// an element is the end.
