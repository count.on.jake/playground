import { Injectable } from '@angular/core';
import { AbstractControl, ValidatorFn } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ValidatorService {

  constructor() { }

  create(config){
    // console.log("validattion here: ", config);
    return config.component === 'input' ? this.required() : undefined;
  }

  required(): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} | null => {
      // const forbidden = true;
      // console.log('required Form Control Validator: ', control);

      return control.value ? null : {'required': {error: 'true'}};
    };
  }
}
