const btnColors = ['btn-primary', 'btn-secondary', 'btn-success', 'btn-danger', 'btn-warning', 'btn-info', 'btn-dark'];

export class Button {

  component;
  name;
  active;
  class;
  attrs;

  constructor(config){
    this.component = config.component;
  }
}
