import { Injectable } from '@angular/core';
import { TextService } from '@playground/random-text';

const Elements = [
  [
    {component: 'input', class: {wrap: '', self: 'form-control', shell: 'basis-two'}},
    {component: 'input', class: {wrap: '', self: 'form-control', shell: 'basis-two'}},
  ],
  [
    {component: 'input'},
  ],
  [
    {component: 'input'},
  ],
  [
    {component: 'input'},
  ],
  [
    {component: 'input'},
  ],
  [
    {component: 'input'},
  ],
  [
    {component: 'select', class: {self: 'form-control', shell: 'basis-two'}, attrs: {type: 'checkbox'}}
  ],
  [
    {component: 'message'}
  ],
  [
    {component: 'button', class: {}, attrs: {type: 'button'}},
  ],
  [
    {component: 'input', class: {wrap: 'ui-checkbox-inline-label-right', self: 'form-control', shell: 'basis-two'}, attrs: {type: 'checkbox'}}
  ]

];
const btnColors = ['btn-primary', 'btn-secondary', 'btn-success', 'btn-danger', 'btn-warning', 'btn-info', 'btn-dark'];
const bgColors = ['bg-primary', 'bg-secondary', 'bg-success', 'bg-danger', 'bg-warning', 'bg-info', 'bg-dark'];
const highFalseRate = [false, false, false, false, false, true];
const basis = ['basis-two', 'basis-three', 'basis-three', 'basis-four', 'basis-five', 'basis-seven', 'basis-ten'];
const inputTypes = ['text', 'text', 'text', 'password', 'email', 'field-text', 'date', 'time', 'month', 'number'];
const labelNames = {
  input: [[1,2,3], [1],[1],[2,2],[2,2]],
  select: [[1,2,3], [1],[1],[2], [2]],
  button: [[1],[1],[2]],
  message: [
    [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
    [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
  ]
}

const css = {
  button: btn,
  input: input,
  select: select,
  message: message,
  link: link
};

// const messageClass = 'p-3 rounded text-white grow-one ' + choose(['basis-five ', 'basis-ten ']) + bgColors[Math.floor(Math.random() * bgColors.length)];
let messageClass;
function message(){
  return {
    class: {
      self: '',
      shell: messageClass
    }
  }
}

function btn(){
  const shell = 'btn ' + btnColors[Math.floor(Math.random() * btnColors.length)]
  return {
    class: {
      self: 'w-100 btn ' + btnColors[Math.floor(Math.random() * btnColors.length)],
      shell: 'rounded basis-two'
    },
    attrs: {
      type: 'button',
      disabled: choose(highFalseRate)
    }
  }
}

function input(){
  return {
    class: {
      self: 'form-control',
      shell: basis[Math.floor(Math.random() * basis.length)]
    },
    attrs: {
      type: inputTypes[Math.floor(Math.random() * inputTypes.length)],
      disabled: choose(highFalseRate)
    }
  }
}

function select(){
  return {
    class: {
      self: 'form-control',
      shell: 'basis-two'
    },
    attrs: {
      disabled: choose(highFalseRate),
      options: buildOptions()
    }
  }
}

function link(){
  return {
    class: {
      self: '',
      shell: ''
    },
    attrs: {
      // disabled: choose(highFalseRate),
      // options: buildOptions()
      data: random()
    }
  }
}

function random(){
  return (Math.random() * 100000).toFixed();
}

function buildOptions(){
  let text = JSON.parse(localStorage.getItem('text'));
  let map = choose([[1,2],[1,2,3],[1,2,3,4],[1,2,3,4,5,6]]).map(i => {
    return {value: choose(text), label: choose(text)};
  });
  return map;
}

function choose(array){
  return array[Math.floor(Math.random()*array.length)];
}

@Injectable()
export class SeedService {

  // Seed service should return
  // a full page with various configurations...
  // Page will have various form for now.
  // and in each form it will have 1 - 10 groups.
  // each group will have 1 - 20 elements in each group.
  // all will have a submit button???
  // maybe a message with each button??? A footer???
  text;
  constructor(private textService: TextService) {
    textService.get('p-11/19-31').subscribe((res: any) => {
      try {
        // this.text = res.text_out.replace(/[<p></p>]/gm, '').split(/\s|\./).filter(word => word);
        localStorage.setItem('text', JSON.stringify(this.text));
        console.info(`TextService fetched ${this.text.length} words.`);
      }catch(e){
        console.error('Please try a reload as a 3rd party api called has failed. [TextService]');
      }
    });
  }


  build(config?){
    // return this.base({component: 'page'});
    return this.page();
  }

  page(){
    // console.log('go!!!!', this.text);
    // this.text = JSON.parse(localStorage.getItem('text'));
    this.text = JSON.parse(localStorage.getItem('text'));
    messageClass = 'p-3 rounded text-white ' + choose(['basis-ten ']) + bgColors[Math.floor(Math.random() * bgColors.length)];


    let page = {
      component: 'page',
      name: this.name(),
      active: true,
      class: choose([{self: '', shell: 'w-75 m-auto flex wrap'}]),
      attrs: {},
      controls: []
    };

    page.controls.push({
        component: 'form',
        name: this.name(),
        active: true,
        class: {self: '', shell: 'w-100'},
        attrs: {},
        controls: [
          {
            component: 'group',
            name: this.name(),
            active: true,
            class: choose([{self: '', shell: 'flex wrap align-end group-shell '}]),
            attrs: {},
            controls: [
              {
                component: 'link',
                name: this.name(),
                active: true,
                class: {self: 'text-white', shell: 'p-2 bg-info rounded mt-4'},
                label: {
                  name: 'Create New Form'
                },
                attrs: {
                  link: ['/forms', this.randomNumber()]
                  // data: this.randomNumber()
                }
              },
              {
                component: 'message',
                name: this.name(),
                active: true,
                label: {
                  name: '(By clicking button a randomly created form will appear)',
                  class: {
                    self: '',
                    shell: ''
                  }
                },
                class: message()
              }
            ]
          }
        ]
      });
    page.controls.push(this.form());

    return page;
  }

  form(){
    let form = {
      component: 'form',
      name: this.name(),
      active: true,
      // class: this.choose([{self: '', shell: 'w-25'}, {self: '', shell: 'w-50'}, {self: '', shell: 'w-75'}, {self: '', shell: 'w-100'}]),
      class: {self: '', shell: 'w-100'},
      attrs: {},
      title: {
        name: this.createLabel(labelNames.input),
        active: true,
        label: {
          name: this.createLabel(labelNames.input),
          class: {
            self: '',
            shell: ''
          }
        },
        class: {
          self: 'h1 border-bottom border-dark'
        }
      },
      controls: []
    };

    choose([[1,2],[1,2,3],[1,2,3,4],[1,2,3,4,5,6],[1,2,3,4,5,6,7,8,9,0,1,2]]).map(i => {
      form.controls.push(this.group());
    });
    return form;
  }

  group(){
    let group = {
      component: 'group',
      name: this.name(),
      active: true,
      class: choose([{self: '', shell: 'flex wrap align-end group-shell newspaper mb-3'}]),
      attrs: {},
      title: {
        name: this.createLabel(labelNames.input),
        active: true,
        label: {
          name: this.createLabel(labelNames.input),
          class: {
            self: '',
            shell: ''
          }
        },
        class: {
          self: 'h4 border-bottom border-dark'
        }
      },
      controls: []
    };

    choose([[1,2,3],[1,2,3,4,5,6,7,8,9],[1,2,3,4,5],[1,2,3,4,5,6,7,8]]).map(i => {
      group.controls.push(this.makeElements(choose(Elements)));
    });
    group.controls = this.flatten(group.controls);
    // console.log('controls: here ', group.controls)
    return group;
  }

  flatten(ary){
   return ary.reduce((obj, value) => obj.concat(value), []);
  }

  createLabel(count){
    // console.log('create label', this.text)
    // let test = this.text.filter(word => {
    //   return !word;
    // });
    // console.log('test sould be ', test, test.length)

    let txt = choose(count).map(i => {
      return choose(this.text);
    });
    return txt.join(' ');
  }

  makeElements(ary){
    return ary.map(ele => {
      return this.element(ele);
    });
  }


  element(ele){
    return {
      component: ele.component,
      name: this.name(),
      active: true,
      attrs: css[ele.component]().attrs || {},
      class: css[ele.component]().class || {},
      label: {
        name: this.createLabel(labelNames[ele.component]),
        class: {}
      }
    }
  }

  randomNumber(){
    return (Math.random() * 100000).toFixed();
  }

  basis(){
    return 'basis-' + choose(['three', 'four', 'five', 'ten']);
  }

  base(config){
    return {
      component: config.component,
      name: this.name(config.component),
      active: true,
      class: {},
      attrs: this.attrs(config.component),
    }
  }

  name(cmp?){
    return this.randomNumber();
  }

  attrs(cmp?){
    return {
      disabled: choose(highFalseRate),
      type: choose(this.specificType(cmp)),
      placeholder: this.randomNumber(),
      id: this.randomNumber()
    }
  }

  specificType(cmp){
    return cmp === 'button' ? 'button' : ['text', 'checkbox', 'number'];
  }
}
