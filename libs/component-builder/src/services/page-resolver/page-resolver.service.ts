import { Injectable } from '@angular/core';
import { RouterStateSnapshot, ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { ComponentBuilderService } from '../component-builder/component-builder.service';
import { Observable } from 'rxjs';


import * as builder from '../../../configs/builder.json';
// @Injectable({
//   providedIn: 'root'
// })

@Injectable()
export class PageResolverService implements Resolve<any> {

  config;
  constructor(private builder: ComponentBuilderService,
  private router: Router){}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any>|Promise<any>|any {
    let page = this.builder.build(builder, this);
    // let page = this.builder.build({component: 'page'}, this);
    page['triggerBuilder'] = true;
    return page;
  }

  createForm(page, form, group, element, e){
    // console.log('build out this here... ', page, ' and event is ', e);
    // page.controls.push(this.builder.build({component: 'form', class: {shell: "w-25 bg-info"}, title: {name: 'hello', label: {name: "wtf we made it"}}}));
    // console.log('clicked new page', this.route);
    this.config = this.builder.build({component: 'page', name: 'newPageThing'});
    // console.log('this router: ', this.router, this.config);
    this.router.navigate(['build/page', this.config.name]);
    // this.route.snapshot.data = this.config;
  }

  createTable(){
    console.log('Make a new Table');
  }
}
