import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidatorService } from '../validator/validator.service';

// NOTE: A component (let's say form), can carry what?
// can carry [title, messages, group, group, message, subtitle, group, actions?]
// then table could???
// title, groupRepeatable, footer]


// basic component has
// active, name, class, etc... and controls
// we could suff a basic with
// controls: [label, input, message, error?]
// error should stay witn input. we could then
// call input.error() to get errors active?
// hello elina.
// How to make new forms?
// button called "New Form!"
// Create new json config file
// so make a generator to create the json.
// Maybe new lib???
// const form = FactoryGirl.create('form');
// This would make a new config file with random items.
// Then just pass in the newly created form into the page controls array?

@Injectable()
export class GroupBuilderService {

  constructor(
    private FB: FormBuilder,
    private validator: ValidatorService
  ){}

  group(controls){
    let ctrls = controls.reduce((obj, item) => {
      obj[item.name] = [
        {value: item.value, disabled: !!item.disabled},
        this.validator.create(item)
        // Validators.required
      ];
      return obj;
    }, {});

    // console.log('build the group here ', controls, ctrls);
    // return ctrls;
    return this.FB.group(ctrls);
    // this.group = FB.group(controls);
  }

  groups(controls){
    let ctrls = controls.reduce((obj, ctrl) => {
      obj[ctrl.name] = ctrl.group;
      return obj;
    }, {});
    return this.FB.group(ctrls);
  }
}
