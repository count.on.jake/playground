import { TestBed, inject } from '@angular/core/testing';

import { GroupBuilderService } from './group-builder.service';

describe('GroupBuilderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GroupBuilderService]
    });
  });

  it('should be created', inject([GroupBuilderService], (service: GroupBuilderService) => {
    expect(service).toBeTruthy();
  }));
});
