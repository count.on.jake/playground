// Core
import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

// This Module
import { HostResolverDirective } from './host-resolver/host-resolver.directive';
import { ComponentBuilderService, RegistryService, GroupBuilderService,
ValidatorService, SeedService, PageResolverService } from './services';

import { RandomTextModule } from '@playground/random-text';
import { UiBuilderDirective } from './ui-builder/ui-builder.directive';
import { ComponentBuilderComponent } from './builder/components/component-builder/component-builder.component';

let registry = null;

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    HttpClientModule,
    RandomTextModule
  ],
  exports: [
    HostResolverDirective,
    UiBuilderDirective,
    ComponentBuilderComponent
  ],
  entryComponents: [
    ComponentBuilderComponent
  ],
  providers: [
    GroupBuilderService,
    ValidatorService,
    SeedService,
    {
      provide: RegistryService, useFactory: () => registry
    }
  ],
  declarations: [
    HostResolverDirective,
    UiBuilderDirective,
    ComponentBuilderComponent
  ]
})

export class ComponentBuilderModule {
  static forRoot(elements): ModuleWithProviders {
    registry = elements;
    return {
      ngModule: ComponentBuilderModule,
      providers: [
        ComponentBuilderService,
        PageResolverService
      ]
    };
  }
}
