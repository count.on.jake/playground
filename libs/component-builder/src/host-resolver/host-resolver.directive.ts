import { Directive, ComponentFactoryResolver, ViewContainerRef,
  OnInit, Input } from '@angular/core';
import { RegistryService } from '../services/registry/registry.service';


@Directive({
  selector: '[resolve-config]'
})

export class HostResolverDirective {
  @Input() input;
  @Input() ctx;

  constructor(
    private resolver: ComponentFactoryResolver,
    private view: ViewContainerRef,
    private registry: RegistryService
  ){}

  ngOnInit(){
    // const cmp = this.resolver.resolveComponentFactory<any>(
    //   this.registry[component.type]);
    // console.log('resolve this: ', this.config)
    const cmp = this.resolver.resolveComponentFactory<any>(
      this.registry[this.input.component]
    );
    // console.log('----))))> ', cmp)
    // console.log('registry', this.registry[this.config.component])
    const resolved = this.view.createComponent(cmp);
    resolved.instance[this.input.component] = this.input;
    if(this.ctx){
      Object.keys(this.ctx).forEach(key => {
        resolved.instance[key] = this.ctx[key];
      })
    }
  }
}
