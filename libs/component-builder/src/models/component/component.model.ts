import { Base } from '../base/base.model';

export class Component extends Base {

  controls;

  constructor(config){
    super(config);
    this.controls = Array.isArray(config.controls) ? config.controls : [];
  }
}
