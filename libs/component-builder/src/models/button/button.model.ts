import { Base } from '../base/base.model';

export class Button extends Base {

  events = {};
  label;

  constructor(config, actions){
    super(config);
    console.log('in button here is actions: ', config, actions)
    this.label = config.label;
    this.events = {click: ()=>{}};
    if(config.events && config.events.click){
      this.events['click'] = actions[(config.events || {}).click].bind(actions) || {};
    }
    // this.events = config.events;
  }
}
