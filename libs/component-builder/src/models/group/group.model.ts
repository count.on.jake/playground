import { Component } from '../component/component.model';
import { Button } from '../button/button.model';
import { Input } from '../input/input.model';
import { Messages } from '../messages/messages.model';
import { Message } from '../message/message.model';
import { Select } from '../select/select.model';
import { Link } from '../link/link.model';
import { Title } from '../title/title.model';

const compFactory = {
  button: Button,
  input: Input,
  message: Message,
  select: Select,
  link: Link,
  title: Title
}

export class Group extends Component {

  controls;
  group;
  title;
  msgs;
  errors;

  constructor(config, actions, GB){
    super(config);
    this.title = config.title || {};
    this.msgs = new Messages(config.messages);
    this.errors = [];

    if(this.controls.length){
      this.controls = this.controls.map(ctrl => {
      return new compFactory[ctrl.component](ctrl, actions);
      });
    }
    this.group = GB.group(this.controls);
  }

  payload(){
    // console.log('creating group payload ', this.group);
    return this.controls.filter(ctrl => {
      // console.log("filter ctrl: ", ctrl)
      return ctrl.hasOwnProperty('payloadKey');
    }).reduce((obj, ctrl) => {
      obj[ctrl.name] = this.group.get(ctrl.name).value;
      return obj;
    }, {});
  }
}
