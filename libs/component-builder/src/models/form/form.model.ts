import { Group } from '../group/group.model';
import { Title } from '../title/title.model';
import { Component } from '../component/component.model';

export class Form extends Component {

  group;
  events;
  submitted;
  title;

  constructor(config, actions, GB){
    super(config); // Base
    console.log('form: ', GB.group([]))

    // this.events = actions[config.events]
    // this.events.greet();
    this.title = new Title(config.title);
    if(this.controls.length){
      this.controls = this.controls.map(ctrl => {
        return new Group(ctrl, actions, GB);
      });
      // this.controls.forEach(ctrl => {
      //   console.log('form builder controls ctrl: ', ctrl)
      //   ctrl = new Group(ctrl, actions, GB);
      // });
    }
    // this.controls = config.controls.map(ctrl => {
    //   // Should we check names here? Throw error if dups?
    //   // This would be the easiest place.
    //   //  const test = this.controls.values('name');
    //   // we could then check test as in
    //   // test.duplicates(); return a boolean?
    //   // get does the following...
    //   // get() returns all ctrls of controls.
    //   // values(key) returns all values of key.
    //   // get(key === value) returns all objects that meet comparitor.
    //   console.log("group iterate: ", actions)
    //   return new Group(ctrl, actions, GB);
    // });
        console.log('form builder before group: ', this.controls)
    this.group = GB.groups(this.controls);
  }

  valid(){
    return this.group.valid;
  }

  payload(){
    // console.log('creating payload for form');
    this.submitted = true;
    return Object.assign({}, ...this.controls.map(ctrl => {
      return ctrl.payload();
    }));
  }
}
