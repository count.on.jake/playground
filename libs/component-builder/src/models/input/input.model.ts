import { Base } from '../base/base.model';

export class Input extends Base {

  events = {};
  label;
  payloadKey;

  constructor(config, actions?){
    super(config);
    this.label = config.label;
    this.payloadKey = config.payloadKey;
  }
}
