export class CssClass {

  self;
  shell;
  wrap;

  constructor(config = <any>{}){
    this.self = config.self || '';
    this.shell = config.shell || '';
    this.wrap = config.wrap || '';
  }
}
