import { Base } from '../base/base.model';

export class Link extends Base {

  events = {};
  label;

  constructor(config, actions?){
    super(config);
    this.label = config.label;
    this.attrs.link = config.attrs.link
  }
}
