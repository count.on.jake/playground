import { CssClass } from '../css-class/css-class.model';
import { Filter } from '../filter/filter.model';

export class Label {

  name;
  class;
  filter;

  constructor(config = <any>{}){
    this.name = config.name || '';
    this.class = new CssClass(config.class);
    this.filter = new Filter(config.filter);
  }
}
