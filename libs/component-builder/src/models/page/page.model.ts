import { Base } from '../base/base.model';
import { Form } from '../form/form.model';
import { Link } from '../link/link.model';
import { Title } from '../title/title.model';

const compFactory = {
  form: Form,
  link: Link
};

export class Page extends Base {

  controls;
  group;
  title;

  constructor(config, actions, FB){
    super(config);
    // check here for control names matching
    // we should do this when ever we make controls
    // this.controls = new Controls(config.controls);
    this.title = new Title(config.title);
    if(Array.isArray(config.controls)){
      this.controls = config.controls.map(ctrl => {
        // return new compFactory[ctrl.component](ctrl, actions[ctrl.events], FB);
        return new compFactory[ctrl.component](ctrl, actions, FB);
      });
    }else{
      this.controls = [];
    }
  }
}
