import { Base } from '../base/base.model';

export class Message extends Base {

  label;

  constructor(config, actions?){
    super(config);
    this.label = config.label;
  }
}
