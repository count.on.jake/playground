import { Base } from '../base/base.model';
import { Label } from '../label/label.model';

export class Element extends Base {

  label;
  constructor(config){
    super(config);
    this.label = new Label(config.label);
  }
}
