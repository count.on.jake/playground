import { Base } from '../base/base.model';

export class Select extends Base {

  events = {};
  label;
  payloadKey;

  constructor(config){
    super(config);
    this.label = config.label;
    this.payloadKey = config.payloadKey;
  }
}
