import { ComponentBuilderModule } from './component-builder.module';

describe('ComponentBuilderModule', () => {
  it('should work', () => {
    expect(new ComponentBuilderModule()).toBeDefined();
  });
});
