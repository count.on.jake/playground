import { Component, OnInit, Input } from '@angular/core';
import * as builder from '../../../../configs/builder.json';

@Component({
  selector: 'component-builder',
  templateUrl: './component-builder.component.html',
  styleUrls: ['./component-builder.component.css']
})
export class ComponentBuilderComponent implements OnInit {

  @Input() input;

  config;
  constructor() { }

  ngOnInit() {
    this.config = builder;
  }
}
