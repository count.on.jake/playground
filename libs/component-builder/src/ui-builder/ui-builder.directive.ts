import { Directive, ComponentFactoryResolver, ViewContainerRef,
  OnInit, Input } from '@angular/core';
import { RegistryService } from '../services/registry/registry.service';
import { ComponentBuilderComponent } from '../builder/components/component-builder/component-builder.component';

const registry = {
  form: ComponentBuilderComponent
}

@Directive({
  selector: '[ui-builder]'
})

export class UiBuilderDirective {
  @Input() input;

  constructor(
    private resolver: ComponentFactoryResolver,
    private view: ViewContainerRef
  ){}

  ngOnInit(){
    // const cmp = this.resolver.resolveComponentFactory<any>(
    //   this.registry[component.type]);
    // console.log('resolve this: ', this.config)
    console.log('one', this.input)
    if(this.input.component){
      const cmp = this.resolver.resolveComponentFactory<any>(
        registry[this.input.component]
      );
      console.log("making this: ", cmp)
      // console.log('----))))> ', cmp)
      // console.log('registry', this.registry[this.config.component])
      const resolved = this.view.createComponent(cmp);
      resolved.instance[this.input.component] = this.input;
    }
    // if(this.ctx){
    //   Object.keys(this.ctx).forEach(key => {
    //     resolved.instance[key] = this.ctx[key];
    //   })
    // }
  }
}
