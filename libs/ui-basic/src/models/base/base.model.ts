export class Base {

  component;
  name;
  active;
  class;
  attrs;

  constructor(config){
    this.component = config.component; // select from only a chosen set.
    // like if(!Registry.has(config.component)){ throw error }
    this.name = config.name || ""; // this needs some work. How do we singularize it?
    // this.name = new Name() cannot be blank, cannot be dup.
    this.active = !(config.active === false);
    this.class = config.class || {}; // new CssClass()
    this.attrs = config.attrs || {}; // Must be object. new Attrs;
  }
}
