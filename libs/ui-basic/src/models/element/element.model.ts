import { Base } from '../base/base.model';

export class Element extends Base {

  events = {};
  label;
  payloadKey;

  constructor(config, actions?){
    super(config);
    this.label = config.label;
    this.events = config.events || {};
    this.payloadKey = config.payloadKey;
  }
}
