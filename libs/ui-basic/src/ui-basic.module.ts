import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { ComponentBuilderModule } from '@playground/component-builder';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faCoffee } from '@fortawesome/free-solid-svg-icons'

library.add(faCoffee);

// This Module Declarations
import {
  PageComponent,
  FormComponent,
  TableComponent,
  TitleComponent,
  MessageComponent,
  MessagesComponent,
  InputComponent,
  ButtonComponent,
  GroupComponent,
  SpanComponent,
  LinkComponent,
  SelectComponent
} from './components';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    RouterModule,
    ComponentBuilderModule.forRoot({
      page: PageComponent,
      form: FormComponent,
      table: TableComponent,
      title: TitleComponent,
      message: MessageComponent,
      input: InputComponent,
      button: ButtonComponent,
      group: GroupComponent,
      span: SpanComponent,
      link: LinkComponent,
      select: SelectComponent
    })
  ],
  exports: [
    PageComponent
  ],
  declarations: [
    PageComponent,
    FormComponent,
    TableComponent,
    TitleComponent,
    InputComponent,
    ButtonComponent,
    GroupComponent,
    SpanComponent,
    LinkComponent,
    SelectComponent,
    MessagesComponent,
    MessageComponent
  ],
  entryComponents: [
    PageComponent,
    FormComponent,
    TableComponent,
    TitleComponent,
    InputComponent,
    ButtonComponent,
    GroupComponent,
    SpanComponent,
    LinkComponent,
    SelectComponent,
    MessageComponent
  ]
})

export class UiBasicModule {}
