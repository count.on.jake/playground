import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'ui-form',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  // @Input() form;
  form;
  constructor(){
    // console.log('this.config in form: ', this)
  }

  ngOnInit() {
    // console.log('ngOnInit: ', this)
  }

}
