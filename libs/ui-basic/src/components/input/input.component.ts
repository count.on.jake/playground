import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'ui-input',
  styleUrls: ['./input.component.scss'],
  template: `
    <div [formGroup]="group.group" [ngClass]="input.class.wrap">
      <label [attr.for]="input.attrs.id" [ngClass]="input.label.class.self">
        {{ input.label.name }}
      </label>
      <input [attr.id]="input.attrs.id" [attr.type]="input.attrs.type"
        [formControlName]="input.name" [ngClass]="input.class.self"
        [attr.placeholder]="input.attrs.placeholder">
    </div>
  `
})
export class InputComponent implements OnInit {

  group
  constructor(private FB: FormBuilder){

  }

  ngOnInit() {
    console.log("input here: !! ", this)
    if(!this.hasOwnProperty('group')){
      this.group = this.FB.group([]);
    }
  }
}
