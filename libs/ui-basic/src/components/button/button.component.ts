import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ui-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {

  button;
  ctx;
  constructor() { }

  ngOnInit() {

  }

  btnClick(e, button){
    console.log('btnClick: ', e)
    // this.button.event.click(e, button, ...this.ctx);
    if(typeof this.button.event === 'function'){
      console.log('action fired')
      // this.ctx.form.events[this.button.event.click]
      this.button.action();
    }
  }
}
