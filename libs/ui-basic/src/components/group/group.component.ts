import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'ui-group',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.scss']
})
export class GroupComponent implements OnInit {

  group;
  constructor() { }

  ngOnInit() {
    // console.log('group ', this.group);
  }
}
