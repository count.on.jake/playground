import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ui-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.css']
})
export class TitleComponent implements OnInit {

  @Input() title;

  constructor() { }

  ngOnInit() {
  }
}
