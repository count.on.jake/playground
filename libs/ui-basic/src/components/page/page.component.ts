import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'ui-page',
  encapsulation: ViewEncapsulation.None,
  template: `
    <div class="" [ngClass]="page.class.shell">

      <ui-title *ngIf="page.title.name" [title]="page.title"></ui-title>

      <fieldset [disabled]="page.attrs.disabled">

        <ng-container *ngIf="page.controls.length">
          <ng-container *ngFor="let ctrl of page.controls;">

            <div *ngIf="ctrl.active" [ngClass]="ctrl.class.shell">
              <ng-container resolve-config [input]="ctrl" [ctx]="{page: page}">
                </ng-container>
            </div>

          </ng-container>
        </ng-container>
        </fieldset>

      </div>
    <router-outlet></router-outlet>
  `
})

export class PageComponent implements OnInit {

  page;
  subscription;
  constructor(private route: ActivatedRoute) {
    console.log('help: ', this, this.route)
  }

  ngOnInit() {
    this.subscription = this.route.data.subscribe(route => {
      console.log('in page ', route, route.data)
      this.page = route.data;
    });
  }

  ngOnDestroy() {
    console.info(`Page: ${this.page.name} - Destroyed.`);
    this.subscription.unsubscribe();
  }
}
