export { UiBasicModule } from './src/ui-basic.module';
export { ComponentBuilderService } from '@playground/component-builder';
export { PageResolverService } from '@playground/component-builder';
export { PageComponent } from './src/components';
